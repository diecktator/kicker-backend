from flask import Flask, request
from flask_restful import Resource, Api
from flask_jsonpify import jsonify
from flask_cors import CORS
from flask_mongoalchemy import MongoAlchemy
from pprint import pprint
import json
from bson import json_util

try:
    from bson.objectid import ObjectId
except:
    pass

app = Flask(__name__)
CORS(app)
app.config['MONGOALCHEMY_DATABASE'] = 'kicker'
app.config['MONGOALCHEMY_USER'] = 'root'
app.config['MONGOALCHEMY_PASSWORD'] = 'root'
api = Api(app)
db = MongoAlchemy(app)

class PlayerDocument(db.Document):
    initials = db.StringField()
    forename = db.StringField()
    lastname = db.StringField()

class Player(Resource):
    def post(self):
        initials = request.json['initials']
        forename = request.json['forename']
        lastname = request.json['lastname']
        player = PlayerDocument(initials=initials, forename=forename, lastname=lastname)
        player.save()
        successMessage = {}
        successMessage["success"] = True
        return json.dumps(successMessage)

    def get(self):
        players = PlayerDocument.query.all()
        players_json = []
        for player in players:
            players_json.append(player.wrap())

        response = app.response_class(
            response=json_util.dumps(players_json),
            status=200,
            mimetype='application/json'
        )
        return response

api.add_resource(Player, '/player')

if __name__ == '__main__':
    app.run(debug=True)